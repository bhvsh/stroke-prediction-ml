# Stroke Prediction using Machine Learning
<a href="https://huggingface.co/spaces/bhvsh/stroke-prediction">![Try it out on Hugging Face Space](https://img.shields.io/badge/%F0%9F%A4%97-Space%20on%20Hub-yellow)</a>

The aim of this project is to develop a model which predicts whether a patient is likely to get a stroke based on the parameters like gender, age various diseases and smoking status.

Dataset used: https://www.kaggle.com/datasets/fedesoriano/stroke-prediction-dataset
