matplotlib
streamlit==1.11.1
pandas==1.1.3
scikit-learn==0.23.2
numpy==1.22.0
lightgbm==3.3.0
